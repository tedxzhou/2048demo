/**
 * Created with IntelliJ IDEA.
 * User: ted
 * Date: 14-4-8
 * Time: 上午11:34
 * To change this template use File | Settings | File Templates.
 */
var logic = (function(){
    var moves = {
        'up': 0,
        'left': 1,
        'down': 2,
        'right': 3
    };

    var bounds = {
        '0': [0, 1, 2, 3],
        '1': [0, 4, 8, 12],
        '2': [12, 13, 14, 15],
        '3': [3, 7, 11, 15]
    };

    var offsets = {
        '0': +4,
        '1': +1,
        '2': -4,
        '3': -1
    };

    var table = [];

    function randomIndex() {
        return (Math.random() * 15) >> 0;
    }

    function randomValue() {
        return Math.random() > 0.8 ? 4 : 2;
    }

    function index1to2(index) {
        // 4 => [1,0]
        var row = (index / 4) >> 0;
        var column = index % 4;
        return [row, column];
    }

    function tableArrayValue(index, value) {
        var rowAndColumn = index1to2(index);
        table[rowAndColumn[0]] = table[rowAndColumn[0]] || [];
        if (value) {
            table[rowAndColumn[0]][rowAndColumn[1]] = value;
        }
        return table[rowAndColumn[0]][rowAndColumn[1]];
    }

    function put() {
        var index = randomIndex();
        while (tableArrayValue(index) != -1) {
            index = randomIndex();
        }
        tableArrayValue(index, randomValue());
    }

    function setup() {
        var i, l;
        for (i = 0, l = 16; i < l; i++) {
            tableArrayValue(i, -1);
        }

        for (i = 0, l = 2; i < l; i++) {
            put();
        }
    }

    function moveTo(move) {
        var moved = false;
        var forward = moves[move];
        var boundArr = bounds[forward];
        var offset = offsets[forward];
        for (var i = 0, l = boundArr.length; i < l; i++) {
            var start = boundArr[i];
            // grab the numbers into a array;
            var currentStart = start;
            var tempArr = [];
            for (var j = 0; j < 4; j++) {
                if (tableArrayValue(currentStart) != -1) {
                    tempArr.push(tableArrayValue(currentStart));
                }
                currentStart = currentStart + offset;
            }
            // combine the same numbers
            for (var j = 0; j < 4; j++) {
                if (tempArr[j + 1] && (tempArr[j] == tempArr[j + 1])) {
                    tempArr[j] *= 2;
                    tempArr.splice(j + 1, 1);
                }
            }

            // reset the table
            currentStart = start;
            for (var j = 0; j < 4; j++) {
                var value = tempArr[j] || -1;
                if(tableArrayValue(currentStart) != value) {
                    moved = true;
                }
                tableArrayValue(currentStart, value);
                currentStart = currentStart + offset;
            }
        }
        return moved;
    }



    function start() {
        setup();
    }


    return {
        table:table,
        start:start,
        moveTo:moveTo,
        put : put
    };
})();
