/**
 * Created with IntelliJ IDEA.
 * User: ted
 * Date: 14-4-8
 * Time: 下午1:57
 * To change this template use File | Settings | File Templates.
 */

var controller = (function() {

//    logic = logic || {};
    var isMoving = false;
    var displayTable = document.getElementById('displayTable');

    function display() {
        var html = "";
        for (var row = 0; row < 4; row++) {
            html += "<tr>";
            for (var column = 0; column < 4; column++) {
                var value =  logic.table[row][column];
                if(value == -1) value = "";
                html += "<td>" + value + "</td>";
            }
            html += "</tr>"
        }

        displayTable.innerHTML = html;
    }

    function move(forward){
        if(isMoving) return;
        if(logic.moveTo(forward)){
            display();
            isMoving = true;
            setTimeout(function () {
                logic.put();
                display();
                isMoving = false;
            },300);
        }
    }
    function start (){
        logic.start();
        display();
    }

    var startPoint = {};
    window.ontouchstart = window.onmousedown = function (e){
        startPoint = {x:e.clientX, y:e.clientY};
    };
    window.ontouchend = window.onmouseup = function(e){
        var diffX = e.clientX - startPoint.x;
        var diffY = e.clientY - startPoint.y;
        var forwardX = diffX > 0 ? 'right' : 'left'; // 左右
        var forwardY = diffY > 0 ? 'down' : 'up'; // 上下
        if(Math.abs(diffX) > Math.abs(diffY)) {
            move(forwardX);
        } else {
            move(forwardY);
        }
    };

    return {
        start:start,
        display:display,
        move:move
    };

})();